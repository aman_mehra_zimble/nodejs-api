const { USER_CONSTANTS } = require("../config/constant.js");
const { validateTestimonialPost, validateTestimonialPut } = require("../models/testimonials");
const Testimonial = require("../models/testimonials");
const _ = require("lodash");
const mongoose = require("mongoose");
const express = require("express");
const multer = require("multer");
const { query } = require("express");
const { update } = require("lodash");

const multerStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public");
  },
  filename: (req, file, cb) => {
    const ext = file.mimetype.split("/")[1];
    cb(null, `files/admin-${file.fieldname}-${Date.now()}.${ext}`);
  }
});

const upload = multer({
  storage: multerStorage
}).any();

module.exports = {
  createTestimonials: async (req, res) => {
    try {
      upload(req, res, async function (err) {
        if (err) {
          // console.log(err);
          return res.send("Error");
        } else {
          var data = req.body;
          var file = req.files;
          // console.log("data", data, file);

          const { error } = await validateTestimonialPost(req.body);
          if (error) return res.status(400).send({ statusCode: 400, message: "Failure", data: { message: error.details[0].message } });

          if (file.length == 0 || file[0].fieldname !== "photo") {
            return res.status(404).send({ statusCode: 404, message: "Photo is required" });
          }
          data.photo = file[0].path;
          Testimonial.addTestimonial(data, async (err, response) => {
            if (err) {
            } else {
              console.log("response", response);
              return res.send({ statusCode: 200, message: "Success", data: response });
            }
          });
        }
      });
    } catch (error) {
      console.log("error", error);
      return res.json({ message: "Internal Server Error" });
    }
  },

  updateTestimonials: async (req, res) => {
    try {
      if (!req.body.id) {
        return res.status(400).send({ statusCode: 400, message: "Failure", data: { message: "id is required." } });
      }
      var testimonial = await Testimonial.findById(req.body.id);
      if (testimonial == null || !testimonial) {
        return res.status(404).send({ statusCode: 404, message: "Failure", data: { message: "Data not found , invalid testimonial id." } });
      }
      var query = { _id: req.body.id };
      var update = {
        $set: {
          name: req.body.name || testimonial.name,
          post: req.body.post || testimonial.post,
          testimonialDescription: req.body.testimonialDescription || testimonial.testimonialDescription,
          lastUpdatedAt: new Date()
        }
      };
      Testimonial.updateTestimonial(query, update, (err, response) => {
        if (err || response == null) {
          return res.status(400).send({ error: err });
        } else {
          return res.status(200).send({ statusCode: 200, message: "Success", data: response });
        }
      });
    } catch (error) {
      console.log(error);
      return res.send({ message: "Internal Server Error" });
    }
  },

  deleteTestimonials: async (req, res) => {
    try {
      if (!req.query.id) {
        return res.status(404).send({
          statusCode: 404,
          message: "Failure",
          data: { message: "Id is required" }
        });
      }
      var query = { _id: req.query.id };
      var update = {
        $set: {
          active: 0
        }
      };
      Testimonial.updateTestimonial(query, update, (err, response) => {
        if (err) {
          return res.send(err);
        } else if (response) {
          console.log(response);
          return res.status(200).send({
            statusCode: 404,
            message: "Success",
            data: { message: "Entry Deleted." }
          });
        }
      });
    } catch (error) {
      console.log(error);
      return res.json({ message: "Internal Server Error" });
    }
  },

  getall: async (req, res) => {
    try {
      var allDetails = await Testimonial.getall();
      if (!allDetails || allDetails.length == 0) {
        return res.send(err);
      } else {
        return res.send({ statusCode: 200, message: "Success", data: { allDetails } });
      }
    } catch (error) {
      console.log("error", error);

      return res.json({ message: "Internal Server Error" });
    }
  }
};
