const config = require("config");
const express = require("express");
var bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json());


app.use(express.static(`${__dirname}/public`));

require("./startup/cors")(app);

require("./startup/mainRoutes")(app);
require("./startup/db")();

const port = process.env.PORT || config.get("port");
const server = app.listen(port, () => console.log(`Listening on port ${port}...`));

module.exports = server;
