const express = require("express");
const testimonials = require("../routes/testimonials");


module.exports = function (app) {
  app.use(express.json());
  app.use("/api/testimonials", testimonials);
  
};
