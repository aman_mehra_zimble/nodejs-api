const cors = require("cors");
module.exports = function (app) {
  var corsOptions = { exposedHeaders: "*", "Access-Control-Allow-Origin": "*" };
  app.use(cors(corsOptions));
};
