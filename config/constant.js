// users.js
const USER_CONSTANTS = {
  INVALID_EMAIL: "Invalid email",
  INVALID_PASSWORD: "Invalid password",
  USER_NOT_FOUND: "User not found",
  EMAIL_ALREADY_EXISTS: "Email already registered",
  USERNAME_ALREADY_EXISTS: "Username already taken, please choose another username.",
  LOGGED_IN: "Logged in successfully",
  DELETED_USER: "You have deleted successfully",
};

// middleware auth
const MIDDLEWARE_AUTH_CONSTANTS = {
  ACCESS_DENIED: "Access denied. No authorization token provided",
  RESOURCE_FORBIDDEN: "You don't have access to the request resource.",
  INVALID_AUTH_TOKEN: "Invalid token",
};

const ATT_CONSTANTS = {
  ALREADY_MARKED_IN: "Already marked IN",
  ALREADY_MARKED_OUT: "Already marked OUT",
  MARKED: "MARKED",
  NO_ENTRY_FOUND: "No entry found",
};

module.exports.USER_CONSTANTS = USER_CONSTANTS;
module.exports.MIDDLEWARE_AUTH_CONSTANTS = MIDDLEWARE_AUTH_CONSTANTS;
module.exports.ATT_CONSTANTS = ATT_CONSTANTS;
