const testimonialsController = require("../controller/testimonialsController");
const express = require("express");

const router = express.Router();

router.post("/create", testimonialsController.createTestimonials);

router.put("/update", testimonialsController.updateTestimonials);

router.put("/delete", testimonialsController.deleteTestimonials);

router.get("/get", testimonialsController.getall);

module.exports = router;
