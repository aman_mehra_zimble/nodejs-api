const config = require("config");
const Joi = require("joi");
const mongoose = require("mongoose");

const testimonial = new mongoose.Schema({
  name: { type: String },
  photo: { type: String },
  post: { type: String },
  testimonialDescription: { type: String },
  active: { type: Number, default: 1 },
  createdAt: { type: Date,default: Date.now, },
  lastUpdatedAt: { type: Date }
});

const Testimonial = mongoose.model("Testimonial", testimonial);
module.exports = {

 validateTestimonialPost:async(testimonial) =>{
  const schema = Joi.object({
    name: Joi.string().min(4).max(50).required(),
    post: Joi.string().required(),

    testimonialDescription: Joi.string().required()
  });

  return schema.validate(testimonial);
},
 validateTestimonialPut:async(testimonial)=> {
  const schema = Joi.object({
    _id: Joi.objectId().required()
  });

  return schema.validate(testimonials);
},

  addTestimonial: async (data,callback) => {
    return Testimonial.create(data,callback);
  },
  updateTestimonial: async (query,update,callback) => {
    return Testimonial.findOneAndUpdate(query,update,{new:true},callback);
  },
  findById: async (data) => {
    return await Testimonial.findOne({_id:data});
  },
  getall: async () => {
    return await Testimonial.find({active:1});
  }
},

exports.Testimonial = Testimonial

